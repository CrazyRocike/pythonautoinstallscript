#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    :   install.py    
@Contact :   sp106168@gmail.com
@License :   (C)Copyright 2014-2019

@Modify Time      @Author    @Version    @Description
------------      -------    --------    -----------
2019/6/4 4:58 PM   moore      1.0         None
"""

# import lib

# coding=utf-8
import os
import sys

# 缓存
temp_dir_path = ""
# python解压路径
python_dir_path = ""
# 下载次数
download_times = 0
# 版本号
python_version = ""


def check_permission():
    """
    权限检查
    :return:
    """
    if os.getuid() == 0:
        pass
    else:
        print('当前用户不是root用户，请以root用户执行脚本')
        sys.exit(1)


def install_dependens():
    """
    安装依赖
    :return:
    """
    cmd = "apt-get install build-essential libsqlite3-dev sqlite3 bzip2 libbz2-dev"
    res = os.system(cmd)
    if res != 0:
        print('安装依赖库失败！')
        sys.exit(1)


def temp_dir():
    """
    创建缓存文件夹
    :return:
    """
    global temp_dir_path
    temp_dir_name = "/temp"
    path = os.getcwd()
    path = path + temp_dir_name
    temp_dir_path = path
    if not os.path.exists(path):
        os.makedirs(path)


def download_python(version):
    """
    下载python
    :param version: python版本
    :return:
    """
    global download_times
    python_url = "https://www.python.org/ftp/python/" + version + "/Python-" + version + ".tgz"
    cmd = 'wget -p' + temp_dir_path + " " + python_url
    res = os.system(cmd)
    if res != 0:
        if download_times < 3:
            download_python(python_version)
            download_times += 1
            print('下载失败尝试第' + str(download_times) + "次下载")
        else:
            print('下载源码包失败，请检查网络')
            sys.exit(1)


def decompression_python(version):
    """
    解压文件包
    :param version:
    :return:
    """
    global download_times, python_dir_path
    python_path = temp_dir_path + "/Python-" + version + ".tgz"
    python_dir_path = temp_dir_path + "/Python-" + version
    cmd = 'tar xf ' + python_path
    res = os.system(cmd)
    if res != 0:
        os.system('rm ' + python_path)
        if download_times < 3:
            download_python(python_version)
            download_times += 1
            print('解压源码包失败，尝试第' + str(download_times) + "次下载")
        else:
            print('解压源码包失败，请重新运行这个脚本下载源码')
            sys.exit(1)


def set_python_version():
    """
    设置版本
    :return:
    """
    global python_version
    python_version = input('请输入你想安装的python版本:')
    if len(python_version) <= 0:
        print('您输入的版本号有误！')
        sys.exit(1)


def update_system():
    """
    更新系统
    :return:
    """
    cmd = "apt-get update"
    res = os.system(cmd)
    if res != 0:
        print('命令：apt-get update 更新系统失败！')
        sys.exit(1)
    cmd = "apt-get upgrade"
    res = os.system(cmd)
    if res != 0:
        print('命令：apt-get upgrade 更新系统失败！')
        sys.exit(1)


def build_python(path):
    """
    编译python
    :param path: python解压路径
    :return:
    """
    cmd = 'cd ' + path + ' && ./configure --prefix=/usr/local/python' + python_version + ' && make && make install'
    res = os.system(cmd)
    if res != 0:
        print('编译python源码失败，请检查是否缺少依赖库')
        sys.exit(1)


def link_python():
    python3_path = "/usr/bin/python3"
    pip3_path = "/usr/bin/pip3"
    if os.path.exists(python3_path):
        cmd = 'sudo mv /usr/bin/python3 /usr/bin/python3_old'
        res = os.system(cmd)
        if res != 0:
            print('重命名Python3 失败')
            sys.exit(1)
    if os.path.exists(pip3_path):
        cmd = 'sudo mv /usr/bin/pip3 /usr/bin/pip3_old'
        res = os.system(cmd)
        if res != 0:
            print('重命名Pip3 失败')
            sys.exit(1)
    cmd = 'sudo ln -s /usr/local/python' + python_version + '/bin/python3 /usr/bin/python3'
    res = os.system(cmd)
    if res != 0:
        print('链接Python3 失败')
        sys.exit(1)
    cmd = 'sudo ln -s /usr/local/python' + python_version + '/bin/pip3 /usr/bin/pip3'
    res = os.system(cmd)
    if res != 0:
        print('链接Python3 失败')
        sys.exit(1)
    print("Python3 安装成功！")


def install_python():
    """
    安装python
    :return:
    """
    check_permission()
    update_system()
    install_dependens()
    temp_dir()
    set_python_version()
    download_python(python_version)
    decompression_python(python_version)
    build_python(python_dir_path)


if __name__ == '__main__':
    temp_dir()

